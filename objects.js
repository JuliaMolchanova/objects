var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30,'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Создаем массив students, добавляем метод show

var commonShow = function () {
    console.log('Студент ' +this.name+ ' набрал ' +this.point+ ' баллов');
};

var students = [];
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
  students.push({name : studentsAndPoints[i], point : studentsAndPoints[i+1], show : commonShow});
};
//console.log(students);


//Добавляем новых студентов

students.push({name: 'Николай Фролов', point: 0, show: commonShow});
students.push({name: 'Олег Боровой', point: 0, show: commonShow});
//console.log(students);


//Увеличиваем баллы некоторым студентам - два способа

//№1

students = students.filter(function (student) {
  if (student.name === 'Ирина Овчинникова' || student.name === 'Александр Малов') {
    return student.point += 30;
  } else
  if (student.name === 'Николай Фролов') {
    return student.point += 10;
  } else {
    return student;
  }
});
//console.log(students);



//№2
/*  for (i = 0, imax = students.length; i < imax; i++) {
	if (students[i].name === 'Ирина Овчинникова') {
  	students[i].point += 30;
  } else
  if (students[i].name === 'Александр Малов') {
  	students[i].point += 30;
  } else
  if (students[i].name === 'Николай Фролов') {
    students[i].point += 10;
  }
};
console.log(students);
*/


//Выводим список студентов, набравших более 30 баллов

console.log('Список студентов, набравших более 30-ти баллов:');
students.forEach(function (student) {
  if (student.point >= 30) {
  student.show();
    }
});

//Выводим поле с количеством сделаннных работ

students.forEach(function (student) {
  student.worksAmount = (student.point / 10);
})
//console.log(students);



//Добавляем метод findByName

students.findByName = function(studentName) {
  var student  = students.find(function (student) {
    return student.name === studentName;
  }); 
  return student;
};
//console.log(students.findByName('Глеб Стукалов'));